const host = "http://localhost:8088";
// const host = "http://gomoku-1312457.xyz:8088";
const api = host + "/api";
const adminApi = api + "/admin";
export const ws = host + "/app";

async function remove(url = '', auth = null) {
    let init = {
        method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        credentials: 'include', // include, *same-origin, omit
    };
    const response = await fetch(url, init);
    if (response.status >= 400) {
        throw await response.json();
    }
    return response.json(); // parses JSON response into native JavaScript objects
}

async function get(url = '', json = true) {
    let init = {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        credentials: 'include', // include, *same-origin, omit
    };
    const response = await fetch(url, init);
    if (response.status >= 400) {
        throw await response.json();
    }
    if (!json) {
        return response.text();
    }
    return response.json(); // parses JSON response into native JavaScript objects
}

async function put(url = '', data = {}) {
    let init = {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'include', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    };
    const response = await fetch(url, init);

    if (response.status >= 400) {
        throw await response.json();
    }
    return response.json(); // parses JSON response into native JavaScript objects
}

async function post(url = '', data = {}) {
    let init = {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'include', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    };
    const response = await fetch(url, init);
    if (response.status >= 400) {
        throw await response.json();
    }
    return response.json(); // parses JSON response into native JavaScript objects
}

const listUser = () => get(`${host}/user`);

const register = (email, displayName, password) => post(`${api}/user/register`, {
    email, password, displayName
});

const updateInfo = (displayName, password) => post(`${api}/user/updateInfo`, {
    password, displayName
});
const authCheck = () => get(`${api}/user/check`);

const login = () => {
    let loginWindow = window.open(`${host}/login`);
    return new Promise(resolve => {
        let interval = setInterval(() => {
            authCheck().then(e => {
                if (e) {
                    loginWindow.close();
                    resolve();
                    clearInterval(interval);
                }
            });
        }, 3000)
    });
}

const userProfile = () => get(`${api}/user/profile`);

const userStats = () => get(`${api}/user/stats`);

const listMatch = () => get(`${api}/match`)

const getMatch = (id) => get(`${api}/match/${id}`)

const listMatchHistory = () => get(`${api}/match/history`)

const createMatch = () => post(`${api}/match`)

const quickJoin = () => post(`${api}/match/quickjoin`)

const joinMatch = (matchId) => post(`${api}/match/${matchId}/join`)

const removeMatch = (matchId) => remove(`${api}/match/${matchId}`)

const currentMatch = () => get(`${api}/match/current`)

const listChat = (matchId) => get(`${api}/match/${matchId}/chat`)

const signOut = () => get(`${host}/logout`)

const adminListChat = (matchId) => get(`${adminApi}/chat/${matchId}`)

const adminListUsers = (email, displayName) => {
    let url = `${adminApi}/users?`;
    if (email) {
        url += `email=${email}&`;
    }
    if (displayName) {
        url += `displayName=${displayName}`;
    }
    return get(url);
}

const adminListMatch = (userId,search) => {
    let url = `${adminApi}/matches?`;
    if (userId) {
        url += `userId=${userId}&`;
    }
    if (search) {
        url += `search=${search}`;
    }
    return get(url);
}

const adminLockUser = (userId) => post(`${adminApi}/lock?userId=${userId}`);

const adminGetMatch = (id) => get(`${adminApi}/match/${id}`)

const adminUserStats = (userId) => get(`${adminApi}/users/${userId}/stats`)

export default {
    ws,
    getMatch,
    listChat,
    authCheck,
    signOut,
    listUser,
    register,
    updateInfo,
    login,
    userProfile,
    userStats,
    listMatch,
    listMatchHistory,
    createMatch,
    currentMatch,
    removeMatch,
    joinMatch,
    quickJoin,
    adminListChat,
    adminListMatch,
    adminListUsers,
    adminUserStats,
    adminLockUser,
    adminGetMatch
};