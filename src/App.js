import './App.css';
import React from "react";
import AuthStore from './components/auth/AuthStore';
import MatchList from "./components/board/MatchList";
import {Redirect, Switch, Route, BrowserRouter} from "react-router-dom";
import Navbar from "./components/navbar/navbar";
import AuthorizationForm from "./components/auth/AuthForm";
import PrivateRoute from "./components/auth/PrivateRoute";
import SocketStore from "./components/board/SocketStore";
import MatchSocket from "./components/board/MatchSocket";
import MatchBoard from "./components/board/MatchBoard";
import RegisterForm from "./components/auth/RegisterForm";
import AuthContext from "./context/AuthContext";
import UpdateInfoForm from "./components/auth/UpdateInfoForm";
import MatchHistory from "./components/board/MatchHistory";
import MatchHistoryBoard from "./components/board/MatchHistoryBoard";
import AdminMainPage from "./components/admin/AdminMainPage";
import AdminMatchHistoryBoard from "./components/admin/AdminMatchHistoryBoard";
import UserStats from "./components/board/UserStats";
import AdminUserStats from "./components/admin/AdminUserStats";


function App() {
    return (
        <BrowserRouter>
            <AuthStore>
                <SocketStore>
                    <Navbar></Navbar>
                    <AuthContext.Consumer>{ctx =>
                        <div className="jumbotron">
                            <h1 className="display-4">React Gomoku!</h1>
                            {ctx.state.profile && !ctx.state.profile.passwordChanged &&
                            <div className="text-danger">We have send a generated password to your social email, please
                                change it</div>
                            }
                            <Switch>
                                <Route exact path="/admin">
                                    <AdminMainPage/>
                                </Route>
                                <Route exact path="/admin/matchhistory/:id">
                                    <AdminMatchHistoryBoard/>
                                </Route>
                                <Route exact path="/admin/stats/:userId">
                                    <AdminUserStats/>
                                </Route>
                                <Route exact path="/login">
                                    <AuthorizationForm/>
                                </Route>
                                <Route path="/register">
                                    <RegisterForm/>
                                </Route>
                                <PrivateRoute>
                                    <MatchSocket></MatchSocket>
                                    <Route path="/stats">
                                        <UserStats/>
                                    </Route>
                                    <Route path="/updateInfo">
                                        <UpdateInfoForm/>
                                    </Route>
                                    <Route exact path="/match/play">
                                        <div>
                                            <MatchBoard/>
                                        </div>
                                    </Route>
                                    <Route exact path="/match">
                                        <div>
                                            <MatchList/>
                                        </div>
                                    </Route>
                                    <Route exact path="/matchhistory">
                                        <div>
                                            <MatchHistory/>
                                        </div>
                                    </Route>
                                    <Route exact path="/matchhistory/:id">
                                        <MatchHistoryBoard/>
                                    </Route>
                                    <Route exact path="/">
                                        <Redirect to="/match"/>
                                    </Route>
                                </PrivateRoute>
                            </Switch>
                        </div>
                    }
                    </AuthContext.Consumer>
                </SocketStore>
            </AuthStore>
        </BrowserRouter>

    );
}

export default App;
