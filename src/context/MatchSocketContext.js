import React from "react";

export default React.createContext({
    state: {
        match: {},
        board: null
    },
    sendMessage: null,
    setSendMessage: () => {

    },
    onMessage: {},
    setOnMessage: () => {
    },
});
