import AuthContext from "../../context/AuthContext";
import {Fragment, useContext} from "react";
import api from "../../api/api";
import {Link, useHistory} from "react-router-dom";

export default function Navbar(props) {
    let history = useHistory();
    const authContext = useContext(AuthContext);

    const navigateToChangeInfo = e=>{
            history.push("/updateInfo");
            e.preventDefault();
    }
    const signOut = (e) => {
        api.signOut().then(() => {
            api.authCheck().then(authenticated => {
                authContext.setState({
                    isAuth: !!authenticated
                })
            });
        });
    }

    return (
        <nav className="navbar navbar-light bg-light justify-content-between">
            <Link to="/" className="navbar-brand">Home</Link>
            <form className="form-inline">
                {authContext.state.isAuth &&
                <Fragment>
                    <div
                        className="m-1 my-sm-0">{authContext.state.profile.displayName + `(${authContext.state.profile.score})`}</div>
                    <button onClick={navigateToChangeInfo} className="btn btn-outline-success my-2 my-sm-0">Update Profile</button>
                    <Link to="/stats" className="btn btn-outline-success m-1 my-sm-0">Details</Link>
                    <Link to="/matchhistory" className="btn btn-outline-success m-1 my-sm-0">History</Link>
                    <button onClick={signOut} className="btn btn-outline-success m-1 my-sm-0">Logout</button>
                </Fragment>
                }
            </form>
        </nav>
    );
}