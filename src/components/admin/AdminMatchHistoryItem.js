import React, {useContext, useEffect} from "react";
import utils from "../../utils";
import {useHistory} from "react-router-dom";

export default function AdminMatchHistoryItem(props) {
    let history = useHistory();
    let match = props;
    let result = match.matchResult === 1 ? "Wins" : match.matchResult === 2 ? "Loses" : "Draws";

    let goToMatchHistoryBoard = () => {
        history.push("admin/matchhistory/" + match.id);
    }
    return (
        <div className="board-item mb-1 mr-1 border bg-white" key={match.id} onClick={goToMatchHistoryBoard}>
            <div className="clearfix">
                <div className="board-player board-player-first float-left m-2"><i
                    className="bi bi-circle-fill"></i>{" " + match.player1.displayName + `(${match.player1ScoreChange})`}
                </div>
                <div className="board-player float-left m-2">{result}</div>
                <div className="board-player board-player-second float-left m-2"><i
                    className="bi bi-circle-fill"></i>{" " + match.player2.displayName + `(${match.player2ScoreChange})`}
                </div>
                <div className="float-right m-2">{utils.formatDate(match.createdAt)}</div>
            </div>
        </div>
    );

}