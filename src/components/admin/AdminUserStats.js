import React, {useEffect, useState} from "react";
import api from "../../api/api";
import {useParams, useHistory} from "react-router-dom";

export default function AdminUserStats(props) {

    let {userId} = useParams();

    let [stats, setStats] = useState(null);

    let [message, setMessage] = useState(null);

    let history = useHistory();

    useEffect(() => {
        api.adminUserStats(userId).then(stats => {
            setStats(stats);
        });
    }, [userId]);
    if (!stats) {
        return (<div>
            Loading...
        </div>)
    }
    const back = () => {
        history.push("/admin");
    }

    const block = () => {
        api.adminLockUser(userId).then(() => {
            setMessage("Block User Successfully!");
        })
    }
    let userStatus = () => {
        if (stats.user.locked) {
            return "Banned";
        }
        if (!stats.user.enabled) {
            return "Wait To Activate";
        }
        return "Active";
    }

    return (
        <div>
            <div className="p-2 mt-1 card border">
                {message && <div className="text-info">{message}</div>}
                <h3 className="model-title"><i className="bi bi-people"></i>User Details: </h3>
                <div>Display Name: {stats.user.displayName}</div>
                <div>Email: {stats.user.email}</div>
                <div>Status: {userStatus()}</div>
                <div>Score: {stats.user.score}</div>
                <div>Total Match Played: {stats.totalMatch}</div>
                <div>Match Win As First Player: {stats.winMatchAsFirstPlayer}</div>
                <div>Match Win As Second Player: {stats.winMatchAsSecondPlayer}</div>
                <div>Match Draw: {stats.drawMatch}</div>
                {
                    stats.totalMatch !== 0 &&
                    <div>Win Rate:
                        {100 * (stats.winMatchAsFirstPlayer + stats.winMatchAsSecondPlayer) / stats.totalMatch}
                    </div>
                }
                <div>
                    <button type="button" className="btn btn-danger m-2" onClick={block}>Block</button>
                    <button type="button" className="btn btn-primary m-2" onClick={back}>Back</button>
                </div>
            </div>
        </div>
    );
}