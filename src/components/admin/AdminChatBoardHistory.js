import React, {useEffect, useState} from "react";
import api from "../../api/api";

export default function AdminChatBoardHistory(props) {

    let currentMatchId = props.id;
    let [chatLines, setChatlines] = useState([]);
    useEffect(() => {
        api.adminListChat(currentMatchId).then(cl => {
            setChatlines(cl);
        })
    }, [props.id]);

    return (
        <div className="board-chat p-2 mb-1 mr-1 border">
            <h2 className="model-title">Chat</h2>
            <div className="board-chat-lines">{chatLines.map(chatline => (
                <div>{chatline.createdBy.displayName}: {chatline.content}</div>
            ))}
            </div>
        </div>
    );
}