import React, {useContext, useEffect, useState} from "react";
import api from "../../api/api";
import {useHistory} from "react-router-dom";

export default function AdminUserList(props) {
    let [users, setUsers] = useState([]);
    let [refresh, setRefresh] = useState(0);
    let history = useHistory();

    useEffect(() => {
        api.adminListUsers().then(users => {
            setUsers(users);
        });
    }, [refresh]);


    const goToUser = (userId) => {
        history.push(`/admin/stats/${userId}`);
    }

    return (
        <div>
            <div className="p-2 mt-1 card border">
                <h3 className="model-title"><i className="bi bi-people"></i>User Lists: ({users.length})</h3>
                <div >{users.map(user => (
                    <div onClick={()=> goToUser(user.id)}>
                        <div>{user.displayName} ({user.score})</div>
                        <div>{user.email}</div>
                    </div>
                ))}
                </div>
            </div>
        </div>
    );
}