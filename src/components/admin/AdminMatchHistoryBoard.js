import React, {useContext, useEffect, useState} from "react";
import utils from "../../utils";
import AuthContext from "../../context/AuthContext";
import api from "../../api/api";
import {useParams, useHistory} from "react-router-dom";
import AdminChatBoardHistory from "./AdminChatBoardHistory";

export default function AdminMatchHistoryBoard(props) {

    let authContext = useContext(AuthContext);
    let history = useHistory();
    let {id} = useParams();
    let [match, setMatch] = useState(null);
    useEffect(() => {
        api.adminGetMatch(id).then(match => {
            setMatch(match);
        });
    }, [id]);
    if (match == null) {
        return (<div>Loading</div>);
    }
    let boardData = match && utils.deserializeBoard(16, match.data);
    let cellEmptyClassNameOrder = true ? "board-empty-first" : "board-empty-second";

    const back = () => {
        history.push("/admin");
    }
    let winStr = () => {
        if(match.matchResult = 0){
            return `Match Draw, Red(${match.player1ScoreChange}), Blue(${match.player2ScoreChange})`;
        }
        if (match.matchResult = 1) {
            return `Red Player Win, Red(${match.player1ScoreChange}), Blue(${match.player2ScoreChange})`;
        }
        if(match.matchResult = 2){
            return `Blue Player Win, Red(${match.player1ScoreChange}), Blue(${match.player2ScoreChange})`;
        }
    }
    return (
        <div className="board-item mb-1 mr-1 card border bg-white" key={props.id}>
            <div className="row no-gutters">
                <div className="col-sm mr-1 p-1 border">
                    <div className="board-status">
                        <i className="bi bi-people"></i>{utils.boardStateToString(match.status)}
                    </div>
                    {match && match.matchResult &&
                    <div className={"text-center "}>{winStr()}</div>
                    }
                    <table className="board">
                        {boardData && boardData.map((cols, i) => (
                            <tr className="board-col">
                                {cols.map((col, j) => (
                                    <td className={"board-cell " + (col === 0 ? cellEmptyClassNameOrder : col === 1 ? "board-first" : "board-second")}/>
                                ))}
                            </tr>
                        ))
                        }
                    </table>
                </div>
                <div className="col-sm p-1">
                    <AdminChatBoardHistory id={match.id}/>
                    <div className="m-1 border mr-1 mb-1 p-2">
                        {(match.status === utils.BOARD_ENDED) &&
                        <button className="btn btn-primary" onClick={back}>Return To Room</button>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}