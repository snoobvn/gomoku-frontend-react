import React, {useContext, useEffect, useState} from "react";
import api from "../../api/api";
import AdminMatchHistoryItem from "./AdminMatchHistoryItem";

export default function AdminMatchList(props) {
    let [matches, setMatches] = useState([]);
    let [search, setSearch] = useState(null);
    const [filteredStates, setFilteredStates] = useState(null);
    let [refresh, setRefresh] = useState(0);

    useEffect(() => {
        const timer = setTimeout(() => {
            api.adminListMatch(null,search).then(matches => {
                setMatches(matches);
            });
        }, 1000);
        return () => clearTimeout(timer);
    }, [search]);

    // useEffect(() => {
    //     api.adminListMatch(null,search).then(matches => {
    //         setMatches(matches);
    //     });
    // }, [refresh, search]);
    let updateSearch = (e)=>{
        setSearch(e.target.value);
    }
    return (
        <div>
            <div className="p-2 mt-1 card border">
                <h3 className="model-title"><i className="bi bi-people"></i>Matches: ({matches.length})</h3>
                <input onChange={updateSearch}/>
                <div>{matches.map(match => (
                    <AdminMatchHistoryItem {...match} ></AdminMatchHistoryItem>
                ))}
                </div>
            </div>
        </div>
    );
}