import React, {useContext, useEffect, useState} from "react";
import api from "../../api/api";
import AdminUserList from "./AdminUserList";
import AdminMatchList from "./AdminMatchList";

export default function AdminMainPage(props) {

    return (
        <div>
            <AdminUserList/>
            <AdminMatchList/>
        </div>
    );
}