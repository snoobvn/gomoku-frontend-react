import React, {useContext, useState} from "react";

import SockJsClient from 'react-stomp';
import {ws} from '../../api/api';
import MatchSocketContext from "../../context/MatchSocketContext";
import {Redirect} from "react-router-dom";

export default function MatchSocket(props) {

    let [clientRef, setClientRef] = useState(null);

    let socketContext = useContext(MatchSocketContext);

    let updateContext = () => {
        console.log("On Connect....",socketContext.setSendMessage);
        socketContext.setSendMessage(() => sendMessage);
    }

    let sendMessage = (dest, payload) => {
        if(clientRef){
            console.log(`Send ${dest}`,payload)
            clientRef.sendMessage(dest, JSON.stringify(payload));
        }else{
            console.log("ClientRef is null");
        }
    };

    return (
        <div>
            {socketContext.state.playing && <Redirect to={{pathname: "/match/play"}}/>}
            <SockJsClient url={ws}
                          topics={[
                              '/topic/online',
                              '/topic/board/start',
                              '/topic/board/join',
                              '/topic/board/state',
                              '/topic/board/created',
                              '/topic/board/message',
                              '/topic/board/removed',
                              '/topic/board/resign',
                              '/topic/board/move']}
                          onMessage={(msg, topic) => {
                              socketContext.onMessage(msg, topic);
                          }}
                          onConnect={() => {
                              updateContext()
                          }}
                          onDisconnect={ ()=>{
                              console.log("Disconnected");
                          }}
                          ref={(client) => {
                              setClientRef(client);
                          }}/>
        </div>
    );
}