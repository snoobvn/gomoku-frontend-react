import React, {useContext, useEffect, useState} from "react";
import api from "../../api/api";
import MatchItem from "./MatchItem";
import MatchHistoryItem from "./MatchHistoryItem";

export default function MatchHistory(props) {

    const [matches, setMatches] = useState([]);

    useEffect(() => {
        api.listMatchHistory().then(matches => {
            setMatches(matches);
        })
    }, []);

    return (
        <div>
            <p className="lead">Your match history</p>
            <div id="boardList">
                <div className="border bg-white mb-2 p-2">
                    {matches.length > 0 ?
                        matches.map(match =>
                            <div className="mb-1 mt-1" key={match.id}>
                                <MatchHistoryItem {...match} />
                            </div>
                        ) :
                        <div className="col text-info">
                            <p>You have not play any match...</p>
                        </div>
                    }
                </div>
            </div>
        </div>
    );

}