import React, {useContext, useEffect, useState} from "react";
import utils from "../../utils";
import AuthContext from "../../context/AuthContext";
import api from "../../api/api";
import ChatBoard from "./ChatBoard";
import {useParams, useHistory} from "react-router-dom";
import ChatBoardHistory from "./ChatBoardHistory";

export default function MatchHistoryBoard(props) {

    let authContext = useContext(AuthContext);
    let history = useHistory();
    let {id} = useParams();
    let [match, setMatch] = useState(null);
    useEffect(() => {
        api.getMatch(id).then(match => {
            setMatch(match);
        });
    }, [id]);
    if (match == null) {
        return (<div>Loading</div>);
    }
    let boardData = match && utils.deserializeBoard(16, match.data);
    let isFirstPlayer = match && authContext.state.profile.id === match.player1.id;
    let mySide = isFirstPlayer ? 1 : 2;
    let cellEmptyClassNameOrder = isFirstPlayer ? "board-empty-first" : "board-empty-second";
    const back = () => {
        history.push("/");
    }

    const isWin = match && match.matchResult === mySide;
    const isDraw = match && match.matchResult === 0;
    const cssWin = () => {
        return isDraw ? 'text-warning' : isWin ? 'text-success' : 'text-danger';
    }
    const winStr = () => {
        let scoreChange = isFirstPlayer ? match.player1ScoreChange : match.player2ScoreChange;
        if (scoreChange > 0) {
            scoreChange = "+" + scoreChange;
        }
        if (isDraw) {
            return `Match is Draw(${scoreChange})`;
        }
        if (isWin) {
            return `You win(${scoreChange})`;
        }
        return `You lose(${scoreChange})`;
    }

    return (
        <div className="board-item mb-1 mr-1 card border bg-white" key={props.id}>
            <div className="row no-gutters">
                <div className="col-sm mr-1 p-1 border">
                    <div className="board-status">
                        <i className="bi bi-people"></i>{utils.boardStateToString(match.status)}
                    </div>
                    {match && match.matchResult &&
                    <div className={"text-center " + cssWin()}>{winStr()}</div>}
                    <table className="board">
                        {boardData && boardData.map((cols, i) => (
                            <tr className="board-col">
                                {cols.map((col, j) => (
                                    <td className={"board-cell " + (col === 0 ? cellEmptyClassNameOrder : col === 1 ? "board-first" : "board-second")}/>
                                ))}
                            </tr>
                        ))
                        }
                    </table>
                </div>
                <div className="col-sm p-1">
                    <ChatBoardHistory id={match.id}/>
                    <div className="border mr-1 mb-1 p-2">
                        {(match.status === utils.BOARD_ENDED) &&
                        <button className="btn btn-primary" onClick={back}>Return To Room</button>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}