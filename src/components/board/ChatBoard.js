import React, {useContext, useEffect, useState} from "react";
import MatchSocketContext from "../../context/MatchSocketContext";
import utils from "../../utils";
import AuthContext from "../../context/AuthContext";
import api from "../../api/api";

export default function ChatBoard(props) {
    let socketContext = useContext(MatchSocketContext);
    let authContext = useContext(AuthContext);
    let currentMatchId = socketContext.state.match ? socketContext.state.match.id : null;
    let [chatLines, setChatlines] = useState([]);
    let [message, setMessage] = useState(null);
    useEffect(() => {
        api.listChat(currentMatchId).then(cl => {
            console.log(cl);
            setChatlines(cl);
        })
    }, [currentMatchId, socketContext.state.chatChanged]);

    let sendMessage = () => {
        socketContext.sendMessage("/board/chat/" + currentMatchId, {content: message});
    }
    let updateMessage = (e) => {
        setMessage(e.target.value);
    }
    return (
        <div className="board-chat p-2 mb-1 mr-1 border">
            <h2 className="model-title">Chat</h2>
            <div className="board-chat-lines">{chatLines.map(chatline => (
                <div>{chatline.createdBy.displayName}: {chatline.content}</div>
            ))}
            </div>
            <div className="input-group mb-3">
                <input className="form-control" type="text" onChange={updateMessage}></input>
                <div className="input-group-append">
                    <input className="form-control" type="submit" onClick={sendMessage}></input>
                </div>
            </div>
        </div>
    );

}