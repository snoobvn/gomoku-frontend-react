import React, {Fragment, useEffect, useState} from "react";
import api from "../../api/api";
import {Link} from "react-router-dom";

export default function UserStats(props) {
    let [stats, setStats] = useState(null);

    useEffect(() => {
        api.userStats().then(stats => {
            setStats(stats);
        });
    }, []);
    if(!stats){
        return (<div>
            Loading...
        </div>)
    }
    let userStatus = () => {
        if (stats.user.locked) {
            return "Banned";
        }
        if (!stats.user.enabled) {
            return "Wait To Activate";
        }
        return "Active";
    }
    return (
        <div>
            <div className="p-2 mt-1 card border">
                <h3 className="model-title"><i className="bi bi-people"></i>User Details: </h3>
                <div>Display Name: {stats.user.displayName}</div>
                <div>Email: {stats.user.email}</div>
                <div>Status: {userStatus()}</div>
                <div>Score: {stats.user.score}</div>
                <div>Total Match Played: {stats.totalMatch}</div>
                <div>Match Win As First Player: {stats.winMatchAsFirstPlayer}</div>
                <div>Match Win As Second Player: {stats.winMatchAsSecondPlayer}</div>
                <div>Match Draw: {stats.drawMatch}</div>
                {
                    stats.totalMatch !== 0 &&
                    <div>Win Rate:
                        {100 * (stats.winMatchAsFirstPlayer + stats.winMatchAsSecondPlayer) / stats.totalMatch}
                    </div>
                }
                <Link to="/" className="btn btn-primary m-1">Back</Link>
            </div>
        </div>
    );
}