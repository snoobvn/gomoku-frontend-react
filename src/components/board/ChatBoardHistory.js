import React, {useContext, useEffect, useState} from "react";
import MatchSocketContext from "../../context/MatchSocketContext";
import utils from "../../utils";
import AuthContext from "../../context/AuthContext";
import api from "../../api/api";

export default function ChatBoardHistory(props) {
    let socketContext = useContext(MatchSocketContext);
    let authContext = useContext(AuthContext);

    let currentMatchId = props.id;
    let [chatLines, setChatlines] = useState([]);
    useEffect(() => {
        api.listChat(currentMatchId).then(cl => {
            setChatlines(cl);
        })
    }, [props.id]);

    return (
        <div className="board-chat p-2 mb-1 mr-1 border">
            <h2 className="model-title">Chat</h2>
            <div className="board-chat-lines">{chatLines.map(chatline => (
                <div>{chatline.createdBy.displayName}: {chatline.content}</div>
            ))}
            </div>
        </div>
    );
}