import React, {Fragment, useContext, useEffect, useState} from "react";
import MatchSocketContext from "../../context/MatchSocketContext";
import utils from "../../utils";
import AuthContext from "../../context/AuthContext";
import api from "../../api/api";

export default function OnlineList(props) {
    let socketContext = useContext(MatchSocketContext);
    let users = socketContext.state.onlineUser;

    return socketContext.state.onlineUser ? (
        <div className="board-chat p-2 mt-1 card border">
            <h3 className="model-title"><i className="bi bi-people"></i>Current Online ({users.length})</h3>
            <div>{users.map(user => (
                <div>{user.displayName} ({user.score})</div>
            ))}
            </div>
        </div>
    ) : <Fragment></Fragment>;

}