import React, {useContext, useEffect} from "react";
import MatchSocketContext from "../../context/MatchSocketContext";
import utils from "../../utils";
import AuthContext from "../../context/AuthContext";
import api from "../../api/api";

export default function MatchItem(props) {
    let socketContext = useContext(MatchSocketContext);
    let authContext = useContext(AuthContext);
    let hasEnoughScore = authContext.state.profile ? authContext.state.profile.score > 10 : null;

    let match = props;
    useEffect(() => {
    }, []);
    let joinMatch = () => {
        api.joinMatch(match.id);
    }
    return (
        <div className="board-item mb-1 mr-1 card border bg-white" key={match.id}>
            <div className="board-item-title">{match.player1.displayName + " waiting"}</div>
            {hasEnoughScore && <button className="btn btn-primary" onClick={joinMatch}>Join Now</button>}
        </div>
    );

}