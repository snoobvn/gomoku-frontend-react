import React, {useContext, useEffect, useState} from "react";
import api from "../../api/api";
import MatchSocketContext from "../../context/MatchSocketContext";
import AuthContext from "../../context/AuthContext";


export default function NewMatchForm({refreshBoardList}) {

    const [error, setError] = useState("");

    let matchSocketContext = useContext(MatchSocketContext);

    let authContext = useContext(AuthContext);

    let hasEnoughScore = authContext.state.profile ? authContext.state.profile.score > 10 : null;

    useEffect(() => {
        console.log(authContext.state.profile);
    }, [matchSocketContext.state.boardCreated]);

    const handle = (e, setFn) => {
        setFn(e.target.value);
    }
    const clear = () => {
        setError(null);
    }

    const createMatch = (e) => {
        api.createMatch()
            .then(success => {
                clear();
                refreshBoardList();
            })
            .catch(error => {
                if (error.code === 6) {
                    setError("Board Name can't be empty");
                } else {
                    setError(error.message);
                }
            });
        e.preventDefault();
    }
    const quickJoin = (e) => {
        api.quickJoin()
            .then(match => {
                clear();
            })
            .catch(error => {
                setError(error.message);
            });
        e.preventDefault();
    }


    return (
        <div className="card">
            {hasEnoughScore ? (
                    <div className="card-body">
                        <div className="text-dark text-center font-weight-bold">
                <span className="align-bottom">
                    Create new board: {matchSocketContext.state.boardCreated}
                </span>
                        </div>
                        <form className="p-2 mt-2 mb-1">
                            <div className="row">
                                <div className="col">
                                    <button className="btn btn-primary btn-block" onClick={createMatch}>New Match</button>
                                    <button className="btn btn-primary btn-block" onClick={quickJoin}>Quick Join</button>
                                </div>
                            </div>
                        </form>
                        {error &&
                        <div className="alert alert-danger mt-2" role="alert">
                            {error}
                        </div>
                        }
                    </div>) :
                (<div className="card-body">
                        <div className="text-dark text-center font-weight-bold">
                            <div>You don't have any more score, please try again tomorrow....</div>
                        </div>
                    </div>
                )}
        </div>)
}