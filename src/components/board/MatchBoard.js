import React, {useContext, useEffect} from "react";
import MatchSocketContext from "../../context/MatchSocketContext";
import utils from "../../utils";
import AuthContext from "../../context/AuthContext";
import api from "../../api/api";
import ChatBoard from "./ChatBoard";
import {Redirect} from "react-router-dom";

export default function MatchBoard(props) {

    let socketContext = useContext(MatchSocketContext);
    let authContext = useContext(AuthContext);
    let currentMatchId = socketContext.state.match ? socketContext.state.match.id : null;

    useEffect(() => {
        if (socketContext.sendMessage && currentMatchId) {
            console.log(socketContext.sendMessage);
            socketContext.sendMessage("/board/state/" + currentMatchId);
        }
    }, [socketContext.sendMessage, currentMatchId]);

    let match = socketContext.state.match;
    let board = socketContext.state.board;
    if (!match) {
        return (<div>Loading...</div>);
    }

    let boardData = board && utils.deserializeBoard(16, board.data);
    let lastSide = board && board.side;
    let currentSide = lastSide === 0 ? 1 : lastSide === 1 ? 2 : 1;
    let isFirstPlayer = authContext.state.profile.id === match.player1.id;
    let mySide = isFirstPlayer ? 1 : 2;
    let isMyTurn = isFirstPlayer ? (mySide !== lastSide || lastSide === 0) : (mySide !== lastSide && mySide !== 0);
    let cellEmptyClassNameOrder = isFirstPlayer ? "board-empty-first" : "board-empty-second";

    const startMatch = () => {
        socketContext.sendMessage(`/board/start/${currentMatchId}`);
    }
    const back = () => {
        socketContext.setState(s => ({
            ...s,
            playing: null
        }));
    }
    const removeMatch = () => {
        api.removeMatch(currentMatchId);
    }

    const resignMatch = () => {
        socketContext.sendMessage(`/board/resign/${currentMatchId}`);
    }
    const boardClick = (i, j) => {
        console.log("Board Click", i, j)
        console.log("First Player", isFirstPlayer)
        console.log("My Turn", isMyTurn)

        if (match.status !== utils.BOARD_PLAYING) {
            return;
        }
        if (!isMyTurn) {
            console.log("Not My Turn")
        }
        socketContext.sendMessage(`/board/move/${currentMatchId}`, {
            i: i, j: j
        });
    };
    const isWin = match.matchResult === mySide;
    const isDraw = match.matchResult === 0;
    const cssWin = () => {
        return isDraw ? 'text-warning' : isWin ? 'text-success' : 'text-danger';
    }
    const winStr = () => {
        let scoreChange = isFirstPlayer ? match.player1ScoreChange : match.player2ScoreChange;
        if (scoreChange > 0) {
            scoreChange = "+" + scoreChange;
        }
        if (isDraw) {
            return `Match is Draw(${scoreChange})`;
        }
        if (isWin) {
            return `You win(${scoreChange})`;
        }
        return `You lose(${scoreChange})`;
    }
    return (
        <div className="board-item mb-1 mr-1 card border bg-white" key={props.id}>
            {!socketContext.state.playing && <Redirect to={{pathname: "/match"}}/>}
            <div className="row no-gutters">
                <div className="col-sm mr-1 p-1 border">
                    <div className="board-status">
                        <i className="bi bi-people"></i>{utils.boardStateToString(match.status)}
                    </div>
                    {(match.status === utils.BOARD_PLAYING || match.status === utils.BOARD_ENDED) &&
                    (<div className="clearfix">
                        <div className="board-player board-player-first float-left"><i
                            className="bi bi-circle-fill"></i>{" " + match.player1.displayName + `(${match.player1.score})`}

                        </div>
                        <div className="board-player board-player-second float-right"><i
                            className="bi bi-circle-fill"></i>{" " + match.player2.displayName + `(${match.player2.score})`}
                        </div>
                    </div>)
                    }
                    {match && match.matchResult &&
                    <div className={"text-center " + cssWin()}>{winStr()}</div>}
                    <table className="board">
                        {boardData && boardData.map((cols, i) => (
                            <tr className="board-col">
                                {cols.map((col, j) => (
                                    <td onClick={() => boardClick(i, j)}
                                        className={"board-cell " + (col === 0 ? cellEmptyClassNameOrder : col === 1 ? "board-first" : "board-second")}/>
                                ))}
                            </tr>
                        ))
                        }
                    </table>
                    <div className={"board-player-" + (currentSide === 1 ? "first" : "second")}>
                        <i className="bi bi-circle-fill"></i>{" " + (currentSide === 1 ? match.player1.displayName : match.player2.displayName)}
                        ({currentSide === 1 ? match.player1.score : match.player2.score})
                    </div>
                </div>
                <div className="col-sm p-1">
                    <ChatBoard/>
                    <div className="border mr-1 mb-1 p-2">
                        {match.status === utils.BOARD_HOLDING && (isFirstPlayer) &&
                        <button className="btn btn-primary m-1" onClick={startMatch}>Start</button>
                        }
                        {match.status === utils.BOARD_PLAYING &&
                        <button className="btn btn-primary m-1" onClick={resignMatch}>Resign</button>
                        }
                        {(match.status === utils.BOARD_HOLDING || match.status === utils.BOARD_WAITING) && (isFirstPlayer) &&
                        <button className="btn btn-primary m-1" onClick={removeMatch}>Cancel Match</button>
                        }
                        {(match.status === utils.BOARD_ENDED) &&
                        <button className="btn btn-primary" onClick={back}>Return To Room</button>
                        }
                    </div>
                </div>
            </div>
        </div>
    );

}