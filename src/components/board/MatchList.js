import React, {useContext, useEffect, useState} from "react";
import api from "../../api/api";
import AuthContext from "../../context/AuthContext";
import NewMatchForm from "./NewMatchForm";
import MatchSocketContext from "../../context/MatchSocketContext";
import {
    Redirect,
    useParams
} from "react-router-dom";
import MatchItem from "./MatchItem";
import OnlineList from "./OnlineList";

export default function MatchList(props) {
    const authContext = useContext(AuthContext);
    const socketContext = useContext(MatchSocketContext);

    const params = useParams();

    const userName = authContext.state.userName;

    const [loading, setLoading] = useState(true);

    const [matches, setMatches] = useState([]);

    useEffect(() => {
        let mounted = true;
        if (authContext.state.isAuth) {
            setLoading(true)
            api.listMatch().then(matches => {
                if (mounted) {
                    setMatches(matches);
                    setLoading(false);
                }
            })
        }
        return () => mounted = false;
    }, [params]);

    useEffect(() => {
        let mounted = true;
        if (authContext.state.isAuth) {
            setLoading(true)
            api.listMatch().then(matches => {
                if (mounted) {
                    setMatches(matches);
                    setLoading(false);
                }
            })
        }
        return () => mounted = false;
    }, [socketContext.state.boardCreated, socketContext.state.matchChanged]);

    return (
        <div>
            {socketContext.state.playing && <Redirect to={{pathname: "/match/play"}}/>}
            <p className="lead">Welcome <b>{userName}</b> to React Gomoku</p>
            <div id="boardList">
                <div className="border bg-white mb-2 p-2 row no-gutters">
                    {loading ?
                        <div className="col text-secondary">
                            <p>Loading, please wait a second...</p>
                        </div>
                        : matches.length > 0 ?
                            matches.map(match =>
                                <div className="col-6 mb-1 mt-1" key={match.id}>
                                    <MatchItem {...match} />
                                </div>
                            ) :
                            <div className="col text-info">
                                <p>There is no match available, please create a match...</p>
                            </div>
                    }
                </div>
                <NewMatchForm/>
                <OnlineList/>
            </div>
        </div>
    );
}