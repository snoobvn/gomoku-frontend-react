import React, {useContext, useEffect, useState} from "react";
import MatchSocketContext from '../../context/MatchSocketContext'
import api from "../../api/api";
import AuthContext from "../../context/AuthContext";

export default function SocketStore(props) {

    const authContext = useContext(AuthContext);

    const [state, setState] = useState({});

    const [sendMessage, setSendMessage] = useState(null);
    useEffect(() => {
        setInterval(() => {
            // ping every one seconds
            if (sendMessage) {
                sendMessage("/online");
            }
        }, 1000);
    }, [sendMessage]);

    useEffect(() => {
        api.currentMatch().then(match => {
            if (match) {
                setState(s => ({
                    ...s,
                    playing: true,
                    match
                }));
            }
        })
    }, [state.matchChanged]);

    const onMessage = (msg, topic) => {
        console.log(topic, msg);
        if (topic === '/topic/online') {
            setState({
                ...state,
                onlineUser: msg
            });
        }
        if (topic === '/topic/board/message') {
            if (msg) {
                setState({
                    ...state,
                    chatChanged: new Date().getMilliseconds()
                });
            }
        }

        if (topic === '/topic/board/created') {
            if (msg) {
                setState({
                    ...state,
                    matchChanged: new Date().getMilliseconds()
                });
            }
        }

        if (topic === '/topic/board/removed') {
            if (msg) {
                setState({
                    ...state,
                    playing: false,
                    matchChanged: new Date().getMilliseconds()
                });
            }
        }

        if (topic === '/topic/board/join') {
            if (msg) {
                setState({
                    ...state,
                    matchChanged: new Date().getMilliseconds()
                });
            }
        }

        if (topic === '/topic/board/state') {
            console.log("Join....", msg)
            if (state.match && state.match.id === msg.match.id) {
                setState({
                    ...state,
                    matchChanged: new Date().getMilliseconds(),
                    match: msg.match,
                    board: msg
                });
            }
        }

        if (topic === '/topic/board/start') {
            console.log("Board Start", msg)
            if (state.match && state.match.id === msg.match.id) {
                setState({
                    ...state,
                    match: msg.match,
                    matchChanged: new Date().getMilliseconds()
                });
            }
        }
        if (topic === '/topic/board/resign') {
            console.log("Board Resign", msg)
            if (state.match && state.match.id === msg.match.id) {
                setState({
                    ...state,
                    match: msg.match,
                    board: msg
                });
            }
            authContext.setState(s=>({
                ...s,
                update: new Date().getMilliseconds()
            }))
        }
        if (topic === '/topic/board/move') {
            if (state.match && state.match.id === msg.match.id) {
                if (msg.moveResult.success)
                    setState({
                        ...state,
                        match: msg.match,
                        board: msg
                    });
            }
            authContext.setState(s=>({
                ...s,
                update: new Date().getMilliseconds()
            }))
        }
    }
    return (
        <MatchSocketContext.Provider value={{
            state, setState,
            sendMessage, setSendMessage,
            onMessage
        }}>
            {props.children}
        </MatchSocketContext.Provider>
    )
}