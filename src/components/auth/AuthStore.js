import React, {useEffect, useState} from "react";
import AuthContext from '../../context/AuthContext'
import api from "../../api/api";

export default function AuthStore(props) {

    const [state, setState] = useState({
        isAuth: false,
        update : null,
        profile: null,
    });

    useEffect(() => {
        api.authCheck().then(authenticated => {
            if (authenticated) {
                api.userProfile()
                    .then(profile => {
                        setState({
                            isAuth: true,
                            profile: profile,
                        })
                    });
            }
        })
    }, [state.update]);

    return (
        <AuthContext.Provider value={{state, setState}}>
            {props.children}
        </AuthContext.Provider>
    )
}