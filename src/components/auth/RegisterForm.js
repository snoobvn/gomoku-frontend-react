import React, {Fragment, useContext, useEffect, useState} from "react";
import api from "../../api/api";
import AuthContext from "../../context/AuthContext";
import {Route, Redirect, useHistory} from "react-router-dom";

export default function RegisterForm() {
    let history = useHistory();
    let authContext = useContext(AuthContext);
    let [displayName, setDisplayName] = useState(null);
    let [email, setEmail] = useState(null);
    let [password, setPassword] = useState(null);
    let [error, setError] = useState(null);
    let [success, setSuccess] = useState(false);

    const updateInput = (e, setFn) => {
        setFn(e.target.value);
    }

    const navigateToLogin = (e) =>{
        history.push("/login");
        e.preventDefault();
    }
    const register = (e) => {
        setError(null);
        api.register(email, displayName, password)
            .then(() => setSuccess(true))
            .catch(e => {
                console.log(e);
                setError(JSON.stringify(e))
            });
        e.preventDefault();
    }

    return (
        <Route>
            {authContext.state.isAuth ?
                <Redirect to="/"></Redirect>
                :
                success ?
                    (<div>
                        <div className="success p-1">Register Success, please check email to activate your account
                            and return login
                        </div>
                        <button type="submit" className="btn btn-primary" onClick={navigateToLogin}>Back To Login</button>
                    </div>) :
                    <form>
                        {error && <div className="text-danger p-1">{error}</div>}
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input className="form-control" id="exampleInputEmail1"
                                   onChange={(e) => updateInput(e, setEmail)}
                                   aria-describedby="emailHelp" placeholder="Enter email"/>
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with
                                anyone else.</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Display Name</label>
                            <input className="form-control" id="exampleInputEmail1"
                                   onChange={(e,) => updateInput(e, setDisplayName)}
                                   aria-describedby="emailHelp" placeholder="Enter email"/>
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with
                                anyone else.</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" id="exampleInputPassword1"
                                   onChange={(e) => updateInput(e, setPassword)}
                                   placeholder="Password"/>
                        </div>
                        <button type="submit" className="btn btn-primary" onClick={register}>Submit</button>
                    </form>
            }
        </Route>
    )
}