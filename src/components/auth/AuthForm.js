import React, {Fragment, useContext, useEffect, useState} from "react";
import api from "../../api/api";
import AuthContext from "../../context/AuthContext";
import {Route, Redirect, useHistory} from "react-router-dom";

export default function AuthorizationForm() {
    const history = useHistory();

    let authContext = useContext(AuthContext);

    const login = (e) => {
        api.login().then(api.userProfile)
            .then(profile => {
                authContext.setState({
                    isAuth: true,
                    profile: profile,
                })
            });
        e.preventDefault();
    }

    const navigateToRegister = () => {
        history.push("/register");
    }

    return (
        <Route>
            {authContext.state.isAuth ?
                <Redirect to="/"></Redirect>
                :
                <div className="btn-group">
                    <button className="btn btn-primary" onClick={login}>Login</button>
                    <button className="btn btn-primary" onClick={navigateToRegister}>Register</button>
                </div>
            }
        </Route>
    )
}