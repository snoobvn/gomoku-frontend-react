import React, {Fragment, useContext, useEffect, useState} from "react";
import api from "../../api/api";
import AuthContext from "../../context/AuthContext";
import {Route, Redirect, useHistory} from "react-router-dom";

export default function UpdateInfoForm() {
    let history = useHistory();
    let authContext = useContext(AuthContext);
    let [displayName, setDisplayName] = useState(null);
    let [password, setPassword] = useState(null);
    let [error, setError] = useState(null);
    let [success, setSuccess] = useState(false);

    const updateInput = (e, setFn) => {
        setFn(e.target.value);
    }

    const navigateToMain = (e) => {
        history.push("/");
        e.preventDefault();

    }
    const updateInfo = (e) => {
        setError(null);
        api.updateInfo(displayName, password)
            .then(() => {
                setSuccess(true);
                authContext.setState(s => ({
                    ...s,
                    update: new Date().getMilliseconds()
                }));
            })
            .catch(e => {
                console.log(e);
                setError(JSON.stringify(e))
            });
        e.preventDefault();
    }

    return (
        <Route>
            {!authContext.state.isAuth ?
                <Redirect to="/"></Redirect>
                :
                success ?
                    (<div>
                        <div className="success p-1">Update Info Successfully</div>
                        <button type="submit" className="btn btn-primary" onClick={navigateToMain}>Back To Login
                        </button>
                    </div>) :
                    <form>
                        {error && <div className="text-danger p-1">{error}</div>}
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Display Name</label>
                            <input className="form-control" id="exampleInputEmail1"
                                   onChange={(e,) => updateInput(e, setDisplayName)}
                                   aria-describedby="emailHelp" placeholder="Enter display name"/>
                            <small id="emailHelp" className="form-text text-muted">Leave blank to keep the old</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" id="exampleInputPassword1"
                                   onChange={(e) => updateInput(e, setPassword)}
                                   placeholder="Password"/>
                            <small id="emailHelp" className="form-text text-muted">Leave blank to keep the old</small>
                        </div>
                        <button type="submit" className="btn btn-primary" onClick={updateInfo}>Submit</button>
                    </form>
            }
        </Route>
    )
}