import AuthContext from "../../context/AuthContext";
import React, {useContext} from "react";
import {Route, Redirect} from "react-router-dom";
import AuthorizationForm from "./AuthForm";

export default function PrivateRoute({children, ...rest}) {
    const authContext = useContext(AuthContext);
    return (
        <Route
            {...rest}
            render={({location}) =>
                authContext.state.isAuth ? (
                    children
                ) : authContext.state.isAuth === false ? (
                    <AuthorizationForm/>
                ) : <div>Auth....</div>
            }
        />
    );
}