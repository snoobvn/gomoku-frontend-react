const BOARD_WAITING = "A_WAITING";
const BOARD_HOLDING = "B_HOLDING";
const BOARD_PLAYING = "C_PLAYING";
const BOARD_ENDED = "D_ENDED";
const deserializeBoard = (boardSize, data) => {
    let points = data.split(" ");
    let pointIndex = 0;
    let result = [];
    for (let i = 0; i < boardSize; i++) {
        result[i] = [];
        for (let j = 0; j < boardSize; j++) {
            result[i][j] = parseInt(points[pointIndex]);
            pointIndex++;
        }
    }
    return result;
}
const boardStateToString = (boardStateToString) => {
    switch (boardStateToString) {
        case BOARD_WAITING:
            return "Wating For Second Player ... ";
            break
        case BOARD_HOLDING:
            return "Waiting For Host to Start Match ...";
            break
        case BOARD_PLAYING:
            return "Playing";
            break
        case BOARD_ENDED:
            return "Ended";
            break
    }
    return "Unknown State";
}

const formatDate = (date) => {
    return new Date(Date.parse(date)).toLocaleString();
}
export default {
    BOARD_WAITING, BOARD_PLAYING, BOARD_HOLDING, BOARD_ENDED,
    formatDate,
    deserializeBoard, boardStateToString
}